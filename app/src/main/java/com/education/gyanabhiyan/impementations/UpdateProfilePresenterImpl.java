package com.education.gyanabhiyan.impementations;

import android.net.Uri;

import com.education.gyanabhiyan.interfaces.UpdateProfileInteractor;
import com.education.gyanabhiyan.interfaces.UpdateProfilePresenter;
import com.education.gyanabhiyan.interfaces.UpdateProfileView;
import com.education.gyanabhiyan.model.User;

public class UpdateProfilePresenterImpl implements UpdateProfilePresenter, UpdateProfileInteractor.OnUpdateFinishedListener {

    private UpdateProfileView updateProfileView;
    private UpdateProfileInteractor updateProfileInteractor;

    public UpdateProfilePresenterImpl(UpdateProfileView updateProfileView) {
        this.updateProfileView = updateProfileView;
        this.updateProfileInteractor = new UpdateProfileInteractorImpl(this);
    }

    @Override
    public void validateCredentials(String name, String phone, String city) {

    }

    @Override
    public void getProfile(User user) {

    }

    @Override
    public void requestCurrentDetails() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void updatePhoto(Uri imagePath) {

    }

    @Override
    public void onUsernameError() {

    }

    @Override
    public void onPhoneError() {

    }

    @Override
    public void onCompanyError() {

    }

    @Override
    public void onDesignationdError() {

    }

    @Override
    public void onCertificateError() {

    }

    @Override
    public void onCityError() {

    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onError() {

    }
}
