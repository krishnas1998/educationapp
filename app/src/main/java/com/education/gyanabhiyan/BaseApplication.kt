package com.education.gyanabhiyan

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.auth.FirebaseAuth
import org.imaginativeworld.oopsnointernet.ConnectionCallback
import org.imaginativeworld.oopsnointernet.NoInternetDialog
import org.imaginativeworld.oopsnointernet.NoInternetSnackbar

open class BaseApplication : AppCompatActivity() {
    // No Internet Dialog
    private var noInternetDialog: NoInternetDialog? = null

    // No Internet Snackbar
    private var noInternetSnackbar: NoInternetSnackbar? = null

    private var mProgressDialog: ProgressDialog? = null

    open fun showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog(this)
            mProgressDialog!!.setCancelable(false)
            mProgressDialog!!.isIndeterminate = true
            mProgressDialog!!.setMessage("Loading...")
        }
        mProgressDialog!!.show()
    }

    open fun hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing) {
            mProgressDialog!!.dismiss()
        }
    }

    open fun hideKeyboard(view: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    }

    open fun getUid(): String? {
        return FirebaseAuth.getInstance().currentUser!!.uid
    }

    open fun getPhoneNo(): String? {
        return FirebaseAuth.getInstance().currentUser!!.phoneNumber
    }
    // ...


    override fun onResume() {
        super.onResume()

        // No Internet Dialog
     /*   noInternetDialog = NoInternetDialog.Builder(this)
                .apply {
                    connectionCallback = object : ConnectionCallback { // Optional
                        override fun hasActiveConnection(hasActiveConnection: Boolean) {
                            // ...
                        }
                    }
                    cancelable = false // Optional
                    noInternetConnectionTitle = "No Internet" // Optional
                    noInternetConnectionMessage =
                            "Check your Internet connection and try again." // Optional
                    showInternetOnButtons = true // Optional
                    pleaseTurnOnText = "Please turn on" // Optional
                    wifiOnButtonText = "Wifi" // Optional
                    mobileDataOnButtonText = "Mobile data" // Optional

                    onAirplaneModeTitle = "No Internet" // Optional
                    onAirplaneModeMessage = "You have turned on the airplane mode." // Optional
                    pleaseTurnOffText = "Please turn off" // Optional
                    airplaneModeOffButtonText = "Airplane mode" // Optional
                    showAirplaneModeOffButtons = true // Optional
                }
                .build()
*/
        // No Internet Snackbar
        noInternetSnackbar =
                NoInternetSnackbar.Builder(this, findViewById(android.R.id.content))
                        .apply {
                            connectionCallback = object : ConnectionCallback { // Optional
                                override fun hasActiveConnection(hasActiveConnection: Boolean) {
                                    // ...
                                }
                            }
                            indefinite = true // Optional
                            noInternetConnectionMessage = "No active Internet connection!" // Optional
                            onAirplaneModeMessage = "You have turned on the airplane mode!" // Optional
                            snackbarActionText = "Settings" // Optional
                            showActionToDismiss = false // Optional
                            snackbarDismissActionText = "OK" // Optional
                        }
                        .build()
    }

    override fun onPause() {
        super.onPause()

        // No Internet Dialog
    /*    noInternetDialog?.destroy()*/

        // No Internet Snackbar
        noInternetSnackbar?.destroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        delegate.localNightMode = AppCompatDelegate.MODE_NIGHT_NO
        val theme = R.style.AppTheme
        setTheme(theme)
    }
/*
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MultiDex.install(this);
    }*/
}