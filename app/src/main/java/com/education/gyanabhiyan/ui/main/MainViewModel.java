package com.education.gyanabhiyan.ui.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.education.gyanabhiyan.BuildConfig;
import com.education.gyanabhiyan.R;
import com.education.gyanabhiyan.adapter.SubjectAdapter;
import com.education.gyanabhiyan.interfaces.IDialog;
import com.education.gyanabhiyan.model.Chapter;
import com.education.gyanabhiyan.model.Grade;
import com.education.gyanabhiyan.model.Subject;
import com.education.gyanabhiyan.model.User;
import com.education.gyanabhiyan.utils.DatabaseUtil;
import com.education.gyanabhiyan.utils.UserData;
import com.education.gyanabhiyan.utils.Utils;
import com.education.gyanabhiyan.utils.firebaseRx.FireBaseDataStoreRx;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

public class MainViewModel extends ViewModel {

    public MainNavigator mainNavigator;
    public User user;
    public MutableLiveData<User> userMutableLiveData;
    public String title;
    public String userName;
    public List<Subject> subjects;
    //public MutableLiveData<List<Subject>> subjectLiveData;
    ObservableArrayList<Subject> subjectLiveData;
    private static final String TAG = "MainViewModel";
    private SubjectAdapter mSubjectAdapter;
    private MutableLiveData<Grade> mGrade;
    public IDialog gradeDialog;
   // public MutableLiveData<String> totalPlayTime;
    public ObservableField<String> totalPlayTime;
    public Long totalVideoPlayTime;
    public Context mContext;
    public MainViewModel() {

        user = new User();
        mSubjectAdapter = new SubjectAdapter(DatabaseUtil.getDatabase().getReference(),new ArrayList<>(),
                this,R.layout.subject_parent_recyclerview,R.layout.subject_chapter_child_recyclerview);
        subjectLiveData = new ObservableArrayList<>();//new MutableLiveData<>();
        mGrade = new MutableLiveData<>();
        totalPlayTime = new ObservableField<>();
    }

    LiveData<User> getUser() {
        if (userMutableLiveData == null) {
            userMutableLiveData = new MutableLiveData<>();
        }

        return userMutableLiveData;
    }
    public void onSubjectClicked(Integer subjectIndex){
        mainNavigator.openSubjectActivity(subjectLiveData.get(subjectIndex),0,user.grade, totalVideoPlayTime);
    }

    public void onChapterClicked(Integer subjectIndex,Integer chapterIndex){
        mainNavigator.openSubjectActivity(subjectLiveData.get(subjectIndex),chapterIndex,user.grade, totalVideoPlayTime);
    }

    public void onShareButtonClicked(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey let's make learning easy at : https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
        sendIntent.setType("text/plain");
//      //  String path = mContext.getResources().getDrawable(R.drawable.gyan_abhiyan_logo,null);
//        Uri uri = Uri.parse("android.resource://"+BuildConfig.APPLICATION_ID+"/drawable/gyan_abhiyan_logo");
//    //    Uri screenshotUri = Uri.parse(path);
//
//        sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
      //  sendIntent.setType("image/*");
        mContext.startActivity(sendIntent);
//        Intent intent = new Intent(Intent.ACTION_VIEW);
//        intent.setData(Uri.parse(
//                "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID));
//        intent.setPackage("com.android.vending");
//        mContext.startActivity(intent);
//        try {
////            Intent shareIntent = new Intent(Intent.ACTION_SEND);
////            shareIntent.setType("text/plain");
////            shareIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
////            String shareMessage= "\nLet me recommend you this application\n\n";
////            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
////            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
////            mContext.startActivity(Intent.createChooser(shareIntent, "choose one"));
//
//            mainNavigator.shareButton();
//        } catch(Exception e) {
//            //e.toString();
//        }
    }

    public SubjectAdapter getAdapter(){
        return mSubjectAdapter;
    }
    public void checkProfileExists(Context context) {
        mContext = context;
        final String uid = Utils.getUid();
        mainNavigator.showProgressDialog();
      //  DatabaseUtil.enablePersistence();
        DatabaseUtil.getDatabase().getReference().child("users").child(uid).child("profile")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mainNavigator.hideProgressDialog();
                        if(!dataSnapshot.exists()) {
                            mainNavigator.openProfileDialog();
                        }
                        else{
                            user = dataSnapshot.getValue(User.class);
                            UserData.getInstance().setUser(user);
                            title = "Welcome " +user.getName();
                            //collapsingToolbarLayout.setTitle("Welcome " + user.name);
                            userName = user.getName();
                            Log.d(TAG, "onDataChange: tille" + title);
                            Log.d(TAG, "onDataChange: userName" + userName);
                            userMutableLiveData.setValue(user);
                            getSubjects(user.grade);

                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public void getTotalPlayTime(){
        //totalPlayTime = new MutableLiveData<>();
        final String uid = Utils.getUid();
        DatabaseUtil.getDatabase().getReference().child("users").child(uid).child("playTime").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.exists()) {

                }
                else {
                    Map<String,Long> totalVideoTime = (Map<String, Long>)dataSnapshot.getValue();
                    Logger.d("TotalPlayTime : "+ totalVideoTime.get("totalVideoTime"));
                    totalVideoPlayTime = totalVideoTime.get("totalVideoTime");
                    if (totalVideoPlayTime != null) {
                        totalPlayTime.set("" + (int)(totalVideoPlayTime/60) + " min");
                     //   totalPlayTime.setValue("" + (int)(totalVideoPlayTime/60) + " min");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public Subject getSubject(Integer subjectIndex)
    {
        if (subjects.size() < subjectIndex)
            return null;
        return subjects.get(subjectIndex);
    }

    public Chapter getChapter(Integer subjectIndex, Integer chapterIndex)
    {
     /*   if (subjects.size() < subjectIndex)
            return null;*/
        for (Chapter chapter :
                subjects.get(subjectIndex).getChapters()) {
            Log.d(TAG,"Chapter Name is : " + chapter.getChapterName());
        }
        return subjects.get(subjectIndex).getChapters().get(chapterIndex);
    }

    @SuppressLint("CheckResult")
    private void getSubjects(String mGrade) {
       // Logger.addLogAdapter(new AndroidLogAdapter());
      //  List<String> subjects = new ArrayList<>();
        this.subjects = new ArrayList<>();
        FireBaseDataStoreRx<Grade> fireBaseDataStoreRx = new FireBaseDataStoreRx<>();
        Observable source = fireBaseDataStoreRx.dataChildEvent(Grade.class,DatabaseUtil.getDatabase().getReference().child("data"));
        if (source != null){
            source.subscribeWith(new DisposableObserver() {
                @Override
                public void onNext(Object o) {
                    if (o instanceof Grade)
                    {
                        Grade grade = (Grade)o;
//                        if (grade == null)
//                            return;
                        MainViewModel.this.mGrade.setValue(grade);
                        Log.v(TAG, "subject :" + grade.getGradeName());
                        Log.d(TAG, "onNext: mGrade : " + mGrade);
                        if (grade.equals(mGrade))
                        {
                            Logger.d(grade.getSubjects());
                            for (Subject subject :
                                    grade.getSubjects()) {
                                // subjects.add(subject.getSubjectName());
                                //mSubjectAdapter.addSubject(subject);

                                subjects.add(subject);
                            }
                            mSubjectAdapter.setSubjects(grade.getSubjects());
                            mSubjectAdapter.notifyDataSetChanged();
                            //subjectLiveData.set(subjectLiveData.size()-1,grade.getSubjects());
                            subjectLiveData.addAll(grade.getSubjects());
                            mainNavigator.updateUI(grade.getSubjects(),user.getName());
                        }
                    }
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });

        }


    }


}
