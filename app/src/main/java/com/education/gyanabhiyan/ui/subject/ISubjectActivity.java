package com.education.gyanabhiyan.ui.subject;

import com.education.gyanabhiyan.model.Chapter;

import java.util.ArrayList;

public interface ISubjectActivity {
    void addChapter(ArrayList<Chapter> chapterList);
}
