package com.education.gyanabhiyan.ui.video


import android.app.Application
import android.content.Context
import android.content.res.Resources
import android.view.View
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.education.gyanabhiyan.R
import com.education.gyanabhiyan.model.History
import com.education.gyanabhiyan.model.Topic
import com.education.gyanabhiyan.utils.DatabaseUtil
import com.education.gyanabhiyan.utils.FirebaseUtils
import com.education.gyanabhiyan.utils.UserData
import com.education.gyanabhiyan.utils.Utils
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ServerValue
import com.google.firebase.database.ValueEventListener
import com.orhanobut.logger.Logger
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerTracker
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.PlayerUiController
import java.util.*
import kotlin.collections.ArrayList


class VideoPlayViewModel(application: Application) : AndroidViewModel(application) {
    private var videoId = ""
    var mTopicName:MutableLiveData<String> = MutableLiveData()
    var topicIndex:MutableLiveData<Int> = MutableLiveData()
    var topicArrayList: ArrayList<Topic> = ArrayList()
    var topicList : MutableList<Topic> = mutableListOf(Topic())
    /*private val topicArrayList = ArrayList<Topic>()*/

    private var mYouTubePlayerView: YouTubePlayerView? = null
    private var mYouTubePlayer: YouTubePlayer? = null
    private var mTracker: YouTubePlayerTracker? = null
    private var totalPlayTime:Long = 0
    private var startTime:Long = 0
    private var bufferingTime:Long = 0
    private var previousTotalPlayTime: Long = 0L
    private var bufferingStartTime:Long = 0
    private var previousState:PlayerConstants.PlayerState = PlayerConstants.PlayerState.UNSTARTED
   /* private val playTime = 0.0f
    private val mBinding: ActivityVideoPlayBinding? = null
    private val mViewModel: VideoPlayViewModel? = null*/
    lateinit var mVideoPlayActivity:IVideoPlayActivity
    private var mContext: Context? = null
    fun init(context: Context, videoPlayActivity:IVideoPlayActivity, topicName:String){
        mContext = context
        mVideoPlayActivity = videoPlayActivity
        mTopicName.value = topicName
        getTotalPlayTime()
    }
    fun initYoutubePlayerView(youTubePlayerView: YouTubePlayerView, videoId: String,startTopicIndex : Int,topicArrayList: ArrayList<Topic>,playTime : Float) {
        this.videoId = videoId
        topicIndex.value = startTopicIndex
        this.topicArrayList = topicArrayList
        topicList.clear()
        topicList.addAll(topicArrayList)
        Logger.d("Topic Index : ${topicIndex.value}")
//        Logger.d("Topic Array List ${this.topicArrayList.size}")
//        for (topic:Topic in topicList)
//            Logger.d("Topic array title: ${topic.topicTitle}")
        mYouTubePlayerView = youTubePlayerView
        mTracker = YouTubePlayerTracker()
        mYouTubePlayerView!!.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) { // String videoId = videoId ; //"S0Q4gqBUs7c";
                mYouTubePlayer = youTubePlayer
                youTubePlayer.loadVideo(videoId, playTime)
                youTubePlayer.addListener(mTracker!!)
            }

            override fun onStateChange(youTubePlayer: YouTubePlayer, playerState: PlayerConstants.PlayerState) {
                if (playerState == PlayerConstants.PlayerState.ENDED) {
                    updateHistory()
                    var endTime = System.currentTimeMillis()
                    totalPlayTime += endTime - startTime - bufferingTime
                    Logger.d("PlayerState.ENDED totalPlayTime : "+ totalPlayTime)
                    previousState = playerState
                    updatePlayTime()
                    Logger.d("topicArrayList size : ${topicArrayList.size}")
                    Logger.d("Topic Index : ${topicIndex.value}")
                    if (topicArrayList.size > topicIndex.value!! + 1) {
                        topicIndex.value = topicIndex.value!! + 1
                        mVideoPlayActivity.updateTopicIndex(topicIndex.value!!)
                        mTopicName.value = topicArrayList[topicIndex.value!!].topicTitle
                        Logger.d("video id : $topicArrayList[topicIndex.value!!].topicVideoId")
                        youTubePlayer.cueVideo(topicArrayList[topicIndex.value!!].topicVideoId, 2f)
                    } else { //Snackbar.make(findViewById(R.id.main_layout),"Finished",Snackbar.LENGTH_SHORT);
                        Toast.makeText(mContext, "Chapter finished", Toast.LENGTH_SHORT).show()
                    }

                }
                else if (playerState == PlayerConstants.PlayerState.PLAYING){
                    startTime = System.currentTimeMillis()
//                    if (previousState == PlayerConstants.PlayerState.BUFFERING)
//                        bufferingTime = System.currentTimeMillis() - bufferingStartTime
                    Logger.d("PlayerState.PLAYING Start Time : "+ System.currentTimeMillis())
                    previousState = playerState
                }
                else if (playerState == PlayerConstants.PlayerState.PAUSED){
                    var endTime = System.currentTimeMillis()
                    totalPlayTime += endTime - startTime
                    Logger.d("PlayerState.PAUSED totalPlayTime : "+ totalPlayTime)
                    bufferingTime = 0L
                    previousState = playerState
                    updatePlayTime()
                }
                else if (playerState == PlayerConstants.PlayerState.BUFFERING){
                    var endTime = System.currentTimeMillis()
                    if(totalPlayTime == 0L)
                        totalPlayTime = 0L
                    else
                        totalPlayTime += endTime - startTime
                    bufferingStartTime = System.currentTimeMillis()
                    Logger.d("bufferingStartTime : "+ bufferingStartTime)
                    previousState = playerState
                }
//                else if (playerState == PlayerConstants.PlayerState.UNSTARTED){
//                    startTime = System.currentTimeMillis()
//                    Logger.d("UNSTARTED time : "+ startTime)
//                }
//                else if (playerState == PlayerConstants.PlayerState.){
//                    var endTime = System.currentTimeMillis()
//                    totalPlayTime = totalPlayTime + endTime - StartTime
//                    Logger.d("totalPlayTime : "+ totalPlayTime)
//                }
            }
        })
        val playerUiController: PlayerUiController = mYouTubePlayerView!!.getPlayerUiController()
        playerUiController.setCustomAction1(getResources().getDrawable(R.drawable.baseline_replay_10_24,null), View.OnClickListener {
            Logger.d( "Current second " + mTracker!!.currentSecond)
            mYouTubePlayer?.seekTo(mTracker!!.currentSecond - 10) //
        })
        playerUiController.setCustomAction2(getResources().getDrawable(R.drawable.baseline_forward_10_24,null), View.OnClickListener {
            Logger.d( "Current second " + mTracker!!.currentSecond)
            mYouTubePlayer?.seekTo(mTracker!!.currentSecond + 10) //
        })
    }

    fun getNextTopic(topicIndex:Int):Topic{
        return if (topicList.size > topicIndex)
            topicList[topicIndex]
        else
            Topic()
//        topicList[topicIndex]?.let { return topicList[topicIndex] }
       // return topicList[topicIndex]
    }

    fun hasNextTopic(topicIndex:Int):Int{
        Logger.d("TopicList Size ${topicList.size}")
        Logger.d("Topic Index $topicIndex")
        return if (topicList.size > topicIndex)
            View.VISIBLE
        else
            View.GONE
    }
    private fun getResources(): Resources{
        return mVideoPlayActivity.resources()
    }

    fun onPause(){
//        var endTime = System.currentTimeMillis()
//        totalPlayTime += endTime - startTime

        updateHistory()

        //
    }

    fun updateTotalPlayTime(){
        Logger.d("onPause : youtube state : " + mTracker?.state)
        if(mTracker?.state == PlayerConstants.PlayerState.PLAYING){
            val endTime = System.currentTimeMillis()
            totalPlayTime += endTime - startTime
            mYouTubePlayer?.pause()
        }
    }

    private fun getTotalPlayTime() {
        val uid = Utils.getUid()
        DatabaseUtil.getDatabase().reference.child("users").child(uid).child("playTime").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                } else {
                    val totalVideoTime = dataSnapshot.value as Map<String, Long>?
                    Logger.d("TotalPlayTime : " + totalVideoTime!!["totalVideoTime"])
                    previousTotalPlayTime = totalVideoTime["totalVideoTime"]!!
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }
    private fun updateHistory(){
        Logger.d("Current second " + mTracker!!.currentSecond)
        if (mTracker!!.currentSecond > 10) {
            //Logger.d("Current second " + mTracker!!.currentSecond)
            Logger.d("Current state " + mTracker!!.state)
            Logger.d("Current Video Duration " + mTracker!!.videoDuration)
            Logger.d("Current Video Id " + mTracker!!.videoId)
            val user = UserData.getInstance().user
            Logger.d("grade : " + user.grade)

            Logger.d("totalVideoTime : " + totalPlayTime)
            //  totalVideoTime["totalVideoTime"] = totalPlayTime
            val history = History("Mathematics", mTracker!!.currentSecond.toString(), mTracker!!.videoId.toString(), (100 * (mTracker!!.currentSecond / mTracker!!.videoDuration)).toString(), mTopicName?.value.toString(), mTracker!!.currentSecond.toString(), ServerValue.TIMESTAMP)
            DatabaseUtil.getDatabase().reference.child("users").child(FirebaseUtils.getCurrentUserId()).child("history").child(user.grade).child(mTracker!!.videoId!!).setValue(history)

        }
    }

    fun updatePlayTime(){
        Logger.d("updatePlayTime called")
        val totalVideoTime = HashMap<String, Any>()
        totalVideoTime["totalVideoTime"] = totalPlayTime/1000 + previousTotalPlayTime
        Logger.d("updatePlayTime: totalVideoTime : " + totalVideoTime["totalVideoTime"])
        DatabaseUtil.getDatabase().reference.child("users").child(FirebaseUtils.getCurrentUserId()).child("playTime").updateChildren(totalVideoTime)
    }
    fun onDestroy(){
        mYouTubePlayerView?.release()
    }
}