package com.education.gyanabhiyan.ui.main;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.education.gyanabhiyan.BaseApplication;
import com.education.gyanabhiyan.R;
import com.education.gyanabhiyan.adapter.SubjectAdapter;
import com.education.gyanabhiyan.databinding.ActivityMainBinding;
import com.education.gyanabhiyan.databinding.NavigationHeaderLayoutBinding;
import com.education.gyanabhiyan.interfaces.IDialog;
import com.education.gyanabhiyan.model.Subject;
import com.education.gyanabhiyan.model.User;
import com.education.gyanabhiyan.ui.about_us.AboutUsActivity;
import com.education.gyanabhiyan.ui.history.HistoryActivity;
import com.education.gyanabhiyan.ui.profile.EditProfileActivity;
import com.education.gyanabhiyan.ui.profile.ProfileActivity;
import com.education.gyanabhiyan.ui.signin.SignInActivity;
import com.education.gyanabhiyan.ui.subject.SubjectActivity;
import com.education.gyanabhiyan.utils.ChooseDialog;
import com.education.gyanabhiyan.utils.Constants;
import com.education.gyanabhiyan.utils.DatabaseUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.FormatStrategy;
import com.orhanobut.logger.Logger;
import com.orhanobut.logger.PrettyFormatStrategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import butterknife.BindArray;
import butterknife.ButterKnife;

public class MainActivity extends BaseApplication implements IDialog, NavigationView.OnNavigationItemSelectedListener, MainNavigator {

    private static final String TAG = MainActivity.class.getName();
    private List<String> subjects;
    private FirebaseAuth mAuth;
/*    @BindView(R.id.collapsingtoolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;*/
    private ImageView profileImageView;
    private TextView profileTextView;
    private Spinner gradeSpinner;
    DrawerLayout drawer;
  /*  @BindViews({ R.id.subject1, R.id.subject2, R.id.subject3 , R.id.subject4})*/
    List<TextView> subjectTextViews;
    private ArrayAdapter<String> gradeAdapter;
    @BindArray(R.array.grades)
    String[] grades;
    private User user;
    private Boolean exit = false;
    private ActivityMainBinding mActivityMainBinding;
    private DrawerLayout mDrawer;
    private MainViewModel mMainViewModel;
    private NavigationView mNavigationView;
    private Toolbar mToolbar;
    private NavigationHeaderLayoutBinding navHeaderMainBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        FormatStrategy formatStrategy = PrettyFormatStrategy.newBuilder()
                .showThreadInfo(false)  // (Optional) Whether to show thread info or not. Default true
                .methodCount(0)         // (Optional) How many method line to show. Default 2
                .methodOffset(7)        // (Optional) Hides internal method calls up to offset. Default 5
                //.logStrategy(customLog) // (Optional) Changes the log strategy to print out. Default LogCat
                .tag("TRACER")   // (Optional) Global tag for every log. Default PRETTY_LOGGER
                .build();
        Logger.addLogAdapter(new AndroidLogAdapter(formatStrategy));
        ButterKnife.bind(this);
        mMainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
//        mActivityMainBinding.(this);
        mMainViewModel.mainNavigator = this;
        mActivityMainBinding.setMainViewModel(mMainViewModel);
        mActivityMainBinding.setLifecycleOwner(this);

        mActivityMainBinding.content.setMainViewModel(mMainViewModel);

        setUp();


        mAuth = FirebaseAuth.getInstance();

        mMainViewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);

        linearLayoutManager.setSmoothScrollbarEnabled(true);
        //AppBarMainLayoutBinding appBarMainLayoutBinding = DataBindingUtil.inflate(getLayoutInflater(),R.layout.app_bar_main_layout,mActivityMainBinding.appbarlayout,false);
        mActivityMainBinding.content.setMainViewModel(mMainViewModel);
        mActivityMainBinding.content.rvParent.setLayoutManager(linearLayoutManager);
        mActivityMainBinding.content.rvParent.setItemAnimator(new DefaultItemAnimator());
        mActivityMainBinding.content.rvParent.setAdapter(new SubjectAdapter(DatabaseUtil.getDatabase().getReference(),new ArrayList<>(),
                mMainViewModel,R.layout.subject_parent_recyclerview,R.layout.subject_chapter_child_recyclerview));


        createNotificationChannel();
        logNotification();
        subscribeWeatherTopics();
        logToken();
        if(savedInstanceState == null){


            mMainViewModel.checkProfileExists(this);

        }


       // animate();
    }

//    public void shareButton(){
//        ShareCompat.IntentBuilder.from(this)
//                .setType("text/plain")
//                .setChooserTitle("Chooser title")
//                .setSubject(getResources().getString(R.string.app_name))
//                .setText("http://play.google.com/store/apps/details?id=" + this.getPackageName())
//                .startChooser();
//    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void animate() {
        YoYo.with(Techniques.SlideInRight)
                .duration(700)

                .playOn(mActivityMainBinding.content.rvParent);
    }

    private void subscribeWeatherTopics() {
        Log.d(TAG, "Subscribing to weather topic");
        // [START subscribe_topics]
        FirebaseMessaging.getInstance().subscribeToTopic("weather")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = getString(R.string.msg_subscribed);
                        if (!task.isSuccessful()) {
                            msg = getString(R.string.msg_subscribe_failed);
                        }
                        Log.d(TAG, msg);
                      //  Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void logNotification() {
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId  = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }
    }

    private void logToken(){
        // Get token
      //  FirebaseMessaging.getInstance().subscribeToTopic("all");
        // [START retrieve_current_token]
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                 //       Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
        // [END retrieve_current_token]
    }

    private void setUp() {
        mDrawer = mActivityMainBinding.drawerView;
        mToolbar = mActivityMainBinding.toolbar;
        mNavigationView = mActivityMainBinding.navigationView;

        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawer,
                mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //hideKeyboard();
            }
        };
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        setupNavMenu();
        /*String version = getString(R.string.version) + " " + BuildConfig.VERSION_NAME;
        mMainViewModel.updateAppVersion(version);
        mMainViewModel.onNavMenuCreated();
        setupCardContainerView();
        subscribeToLiveData();*/
    }

    private void setupNavMenu() {
        navHeaderMainBinding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.navigation_header_layout, mActivityMainBinding.navigationView, false);
        mActivityMainBinding.navigationView.addHeaderView(navHeaderMainBinding.getRoot());
        navHeaderMainBinding.setMainViewModel(mMainViewModel);
        navHeaderMainBinding.setGradeDialog(this);
        mNavigationView.setNavigationItemSelectedListener(
                item -> {
                    mDrawer.closeDrawer(GravityCompat.START);
                    switch (item.getItemId()) {
                        case R.id.profile:
                            startActivity(new Intent(this, ProfileActivity.class));
                            return true;
                        case R.id.history:
                            //logToken();
                            startActivity(new Intent(this, HistoryActivity.class));
                            return true;
                        case R.id.about_us:
                            startActivity(new Intent(this, AboutUsActivity.class));
                            return true;
//                        case R.id.add_video:
//                            startActivity(new Intent(this, AddVideoActivity.class));
//                            return true;
                        case R.id.log_out:
                            signOut();
                            return true;
                        default:
                            return false;
                    }
                });
    }
    @Override
    protected void onStart() {
        super.onStart();
        mMainViewModel.getTotalPlayTime();
    }

    public void updateUI(List<Subject> subjects, String userName) {
        mActivityMainBinding.collapsingtoolbar.setTitle("Welcome " + userName);
        mActivityMainBinding.invalidateAll();
        navHeaderMainBinding.invalidateAll();
    }

    @Override
    public void onStarted() {

    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void setProfileImage(String message) {

    }
    @Override
    public void openProfileDialog() {
        startActivity(new Intent(this, EditProfileActivity.class));
    }

    private void signOut() {
        mAuth.signOut();
        //updateUI(STATE_INITIALIZED);
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();
        switch (id){
            case R.id.profile:
                startActivity(new Intent(this, ProfileActivity.class));
                finish();
                break;
            case R.id.log_out:
                signOut();
                break;
            //case R.id.
            default:
                break;
        }
        return true;
    }

    @Override
    public void openSubjectActivity(Subject subject, Integer chapterIndex, String grade, Long totalVideoTime){
        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, SubjectActivity.class);
        bundle.putString("subjectName",subject.getSubjectName());
        bundle.putInt(Constants.CHAPTER_INDEX,chapterIndex);
        bundle.putString("grade",grade);
        bundle.putParcelable("subject",subject);
        intent.putExtras(bundle);
        startActivity(intent);
      //  finish();
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, R.string.pressbackagain,
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    @Override
    public void inflateDialog(int dialogType) {
        ChooseDialog dialog = new ChooseDialog(Arrays.asList(grades),1);
        dialog.show(getSupportFragmentManager(), getString(R.string.dialog_choose_grade));
    }

    @Override
    public void setDialogValue(String value, int dialogType) {
        setGrade(value);
    }

    public void setGrade(String grade) {
        Logger.d("Selected grade is : " + grade);
        User user = mMainViewModel.userMutableLiveData.getValue();
        if (grade.equals(user.grade))
            return;
        user.grade = grade;
        mMainViewModel.userMutableLiveData.setValue(user);
        updateProfile(grade);
       // mActivityMainBinding.setQuantity(grade);
    }

    public void updateProfile(String grade) {
        final String uid = getUid();
        Map<String, Object> postValues = new HashMap<String,Object>();
        DatabaseReference profileRefrence = FirebaseDatabase.getInstance().getReference().child("users").child(uid).child("profile");
//        User user = userMutableLiveData.getValue();
//        postValues.put("email", user.email);
//        postValues.put("name", user.getName());
//        postValues.put("location", user.location);
        postValues.put("grade", grade);

      //  postValues.put("gender", user.gender);

//        if(!validate(user,view))
//            return;
//        UserData.getInstance().setUser(user);
        profileRefrence.updateChildren(postValues);
        startActivity(new Intent(this, MainActivity.class));
        finish();
      //  iEditProfileActivity.goBack();
    }
    @Override
    public void goBack() {

    }
}


