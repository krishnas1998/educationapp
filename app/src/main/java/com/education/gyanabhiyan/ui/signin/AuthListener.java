package com.education.gyanabhiyan.ui.signin;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;

public interface AuthListener {
    void onStarted();
    void onSuccess();
    void onFailure(String message);
    void startPhoneNumberVerification(String phoneNo);
    void setPhoneNoError(String message);
    void setVerificationError(String message);
    void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred);
}
