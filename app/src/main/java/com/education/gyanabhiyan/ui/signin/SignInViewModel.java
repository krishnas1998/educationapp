package com.education.gyanabhiyan.ui.signin;

import android.app.Activity;
import android.app.Application;
import android.util.Log;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import androidx.lifecycle.AndroidViewModel;

public class SignInViewModel extends AndroidViewModel {
    public String phoneNo;
    public String verficationCode;
    public AuthListener authListener;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private static final String TAG = "SignInViewModel";
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private FirebaseAuth mAuth;
    public Activity activity;
    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";
    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;
    private String countryCode = "+91";
    public boolean showProgress = false;
//    private Countr
  //  private final Context mContext; // To avoid leaks, this must be an Application Context.
 //   private final SnackbarMessage mSnackbarText = new SnackbarMessage();

    public SignInViewModel(Application context) {
        super(context);
        mAuth = FirebaseAuth.getInstance();
    //    mContext = context.getApplicationContext(); // Force use of Application Context.
        //activity = (Activity)mContext;
    }


    public void onLoginClicked(View view){
        authListener.onStarted();
        Log.d(TAG, "onLoginClicked: "+phoneNo);
        if (phoneNo == null || !validatePhoneNumber()){
            //error
            //authListener.onFailure("Error");
            return;
        }

        authListener.startPhoneNumberVerification(phoneNo);
        //authListener.onSuccess();
    }
    private boolean validatePhoneNumber() {
        if (phoneNo == null || phoneNo.isEmpty()) {
            //mPhoneNumberField.setError("Invalid phone number.");
            authListener.setPhoneNoError("Error");
            authListener.onFailure("Phone no cannot be empty");
            return false;
        }
//        else if (phoneNo.length() != 10){
//            authListener.setPhoneNoError("Error");
//            authListener.onFailure("Invalid phone no.");
//        }
        return true;
    }

    public void updateUI(int uiState) {
        authListener.updateUI(uiState, mAuth.getCurrentUser(), null);
    }

    public void updateUI(FirebaseUser user) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user);
        } else {
            updateUI(STATE_INITIALIZED);
        }
    }

    public void updateUI(int uiState, FirebaseUser user) {
        authListener.updateUI(uiState, user, null);
    }

    public void updateUI(int uiState, PhoneAuthCredential cred) {
        authListener.updateUI(uiState, null, cred);
    }
   /* SnackbarMessage getSnackbarMessage() {
        return mSnackbarText;
    }

    private void showSnackbarMessage(Integer message) {
        mSnackbarText.setValue(message);
    }*/
}
