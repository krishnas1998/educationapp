package com.education.gyanabhiyan.ui.profile;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.education.gyanabhiyan.R;
import com.education.gyanabhiyan.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import static com.education.gyanabhiyan.utils.Utils.getPhoneNo;
import static com.education.gyanabhiyan.utils.Utils.getUid;

public class ProfileViewModel extends ViewModel {
    public String phoneNo;
    private DatabaseReference mProfile;
    private User user;
    public ProfileNavigation mProfileNavigation;
    public ObservableField<String> resultImageUrl = new ObservableField<>();
    private FirebaseAuth mAuth;
    public MutableLiveData<User> userMutableLiveData;

    LiveData<User> getUser() {
        if (userMutableLiveData == null) {
            userMutableLiveData = new MutableLiveData<>();
        }

        return userMutableLiveData;
    }
    public ProfileViewModel() {
        user = new User();
        mAuth = FirebaseAuth.getInstance();
    }

    public void getProfileDetails() {
        final String uid = getUid();

        phoneNo = getPhoneNo();
        mProfile = FirebaseDatabase.getInstance().getReference().child("users").child(uid).child("profile");
        mProfile.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get user information
                if(dataSnapshot.exists()) {
                    user = dataSnapshot.getValue(User.class);
                    if(user != null) {
                       /* Glide.with(getContext())
                                .load(user.getPhotoUrl())
                                .placeholder(R.drawable.baseline_person_24)

                                .into(mPhotoImgageView);*/
                        userMutableLiveData.setValue(user);
                        resultImageUrl.set(user.getPhotoUrl());

                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void onEditProfileClicked(View view){
        mProfileNavigation.openEditProfileActivity(userMutableLiveData.getValue());
    }

    public void onSignOutClicked(View view){
        mProfileNavigation.signOut();
    }
    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .placeholder(R.drawable.baseline_person_24)
                .into(view);
    }

   /* public String getImageUrl() {
        // The URL will usually come from a model (i.e Profile)
        return userMutableLiveData.getValue().photoUrl;
    }*/



}
