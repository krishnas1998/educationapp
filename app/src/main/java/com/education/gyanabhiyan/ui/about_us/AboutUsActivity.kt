package com.education.gyanabhiyan.ui.about_us

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.education.gyanabhiyan.R
import com.education.gyanabhiyan.ui.main.MainActivity

class AboutUsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)

        // my_child_toolbar is defined in the layout file
        // getProfileDetails();
// my_child_toolbar is defined in the layout file
        val myChildToolbar = findViewById<View>(R.id.my_child_toolbar) as Toolbar
        setSupportActionBar(myChildToolbar)

        // Get a support ActionBar corresponding to this toolbar
        // Get a support ActionBar corresponding to this toolbar
        val ab = supportActionBar

        // Enable the Up button
        // Enable the Up button
        ab!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
