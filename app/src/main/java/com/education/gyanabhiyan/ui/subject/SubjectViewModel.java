package com.education.gyanabhiyan.ui.subject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.education.gyanabhiyan.R;
import com.education.gyanabhiyan.adapter.ChapterAdapter;
import com.education.gyanabhiyan.model.Chapter;
import com.education.gyanabhiyan.model.History;
import com.education.gyanabhiyan.model.Subject;
import com.education.gyanabhiyan.model.Topic;
import com.education.gyanabhiyan.ui.video.VideoPlayActivity;
import com.education.gyanabhiyan.utils.Constants;
import com.education.gyanabhiyan.utils.DatabaseUtil;
import com.education.gyanabhiyan.utils.FirebaseUtils;
import com.education.gyanabhiyan.utils.UserData;
import com.education.gyanabhiyan.utils.firebaseRx.FireBaseDataStoreRx;
import com.orhanobut.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.PopupMenu;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

import static com.education.gyanabhiyan.utils.Constants.TOPIC_LIST;

public class SubjectViewModel extends ViewModel implements ISubjectActivity{
    private ChapterAdapter mChapterAdapter;
    public Context mContext;
    private String mGrade, mSubjectName;
    private Subject mSubject;
    private MutableLiveData<List<Chapter>> mChapters;
    private ArrayList<History> historyList;
    private final String TAG = SubjectViewModel.class.getName();
    LinearLayoutManager mManager;
    List<Chapter> chapterList;
    int chapterIndex;
    public void init(Context context, Subject subject
                     , LinearLayoutManager mManager, int chapterIndex){
        mContext = context;
        mSubject = subject;
        mSubjectName = subject.getSubjectName();
        mChapters = new MutableLiveData<>();
        chapterList = new ArrayList<>();
        this.mManager = mManager;
        this.chapterIndex = chapterIndex;
        mChapterAdapter = new ChapterAdapter(mSubject, this, R.layout.chapter_parent_recyclerview, R.layout.chapter_child_recyclerview);
        this.mManager.scrollToPositionWithOffset(chapterIndex, 0);
    }
    public ChapterAdapter getAdapter(){
        return mChapterAdapter;
    }

    @Override
    public void addChapter(ArrayList<Chapter> chapterList) {
        this.chapterList = chapterList;
        mChapters.setValue(chapterList);

    }

    private List<String> historyId ;
    @SuppressLint("CheckResult")
    public void  getHistory(){
        historyList = new ArrayList<>();
        historyId = new ArrayList<>();
        FireBaseDataStoreRx<History> fireBaseDataStoreRx = new FireBaseDataStoreRx<>();
        Observable source =  fireBaseDataStoreRx.dataValueEvent(History.class,DatabaseUtil.getDatabase().getReference("users").child(FirebaseUtils.getCurrentUserId()).child("history").child(UserData.getInstance().user.grade));
        if (source != null) {
            source.subscribeWith(new DisposableObserver() {
                @Override
                public void onNext(Object o) {
                    if (o instanceof History){
                        History history = (History) o;
                        historyId.add(history.getVideoId()) ;
                        historyList.add(history);
                        Logger.d("Progress : " + history.getVideoProgress());

                    }
                }

                @Override
                public void onError(Throwable e) {
                Logger.e("History error" + e);
                }

                @Override
                public void onComplete() {
                    mChapterAdapter.notifyDataSetChanged();
                    Logger.d("history complete : ");
                }
            });
        }
    }

    public int getProgress(Integer chapterIndex, Integer topicIndex){
        History history = getHistory(chapterIndex,topicIndex);
        return history == null ? 0 :Float.valueOf(history.getVideoProgress()).intValue();
    }

    public History getHistory(Integer chapterIndex, Integer topicIndex){
        Topic topic = getTopic(chapterIndex,topicIndex);
        if (historyList != null){
            int index = historyId.indexOf(topic.getTopicVideoId());
            if (index != -1)
                return historyList.get(index);
        }
        return null;
    }

    public void destroy()
    {
        Logger.d("destroy");
        historyList = null;
    }
    public Chapter getChapter(Integer chapterIndex){
        Chapter chapter= mChapters.getValue().get(chapterIndex);
        return chapter;
    }

    public Topic getTopic(Integer chapterIndex, Integer topicIndex){

        Topic topic= mChapters.getValue().get(chapterIndex).getChapterTopics().size()>topicIndex ? mChapters.getValue().get(chapterIndex).getChapterTopics().get(topicIndex) : null;
        return topic;
    }

    public void onTopicClick(Integer chapterIndex, Integer topicIndex){
        playVideo(chapterIndex,topicIndex,false);
    }

    public void playVideo(Integer chapterIndex,Integer topicIndex, boolean resume){
        Chapter chapter= mChapters.getValue().get(chapterIndex);
        Topic topic = chapter.getChapterTopics().get(topicIndex);
        Log.v(TAG,"Title :" + topic.getTopicTitle());
        Intent intent = new Intent(mContext, VideoPlayActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.VIDEO_ID,topic.getTopicVideoId());
        bundle.putInt(Constants.TOPIC_INDEX,topicIndex);
        bundle.putParcelableArrayList(TOPIC_LIST,chapter.getChapterTopics());
        bundle.putString(Constants.TOPIC_NAME,topic.getTopicTitle());
        if (resume)
            bundle.putFloat(Constants.START_TIME,Float.valueOf(getHistory(chapterIndex,topicIndex).getCurrentSecond()));
        else
            bundle.putFloat(Constants.START_TIME,0.0f);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }
    public void showPopupMenu(Integer chapterIndex, Integer topicIndex, View view){
        PopupMenu popup = new PopupMenu(view.getContext(),view );
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_menu, popup.getMenu());
       // popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position));
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.resume:

                        playVideo(chapterIndex,topicIndex,true);
                }
                return true;
            }
        });
        popup.show();
    }

    public void onPlayListClick(Integer chapterIndex) {
        Chapter chapter = mChapters.getValue().get(chapterIndex);
        Topic topic = chapter.getChapterTopics().get(0);
        Log.v(TAG, "Title :" + topic.getTopicTitle());
        Intent intent = new Intent(mContext, VideoPlayActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.VIDEO_ID, topic.getTopicVideoId());
        bundle.putInt(Constants.TOPIC_INDEX, 0);
        bundle.putParcelableArrayList(TOPIC_LIST, chapter.getChapterTopics());
        bundle.putString(Constants.TOPIC_NAME,topic.getTopicTitle());
        bundle.putFloat(Constants.START_TIME,0.0f);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }

}