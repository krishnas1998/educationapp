package com.education.gyanabhiyan.ui.video;

import android.content.res.Resources;
import android.os.Bundle;

import com.education.gyanabhiyan.BaseApplication;
import com.education.gyanabhiyan.R;
import com.education.gyanabhiyan.ViewModelFactory;
import com.education.gyanabhiyan.databinding.ActivityVideoPlayBinding;
import com.education.gyanabhiyan.model.Topic;
import com.education.gyanabhiyan.utils.Constants;
import com.orhanobut.logger.Logger;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerTracker;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import static com.education.gyanabhiyan.utils.Constants.VIDEO_ID;

public class VideoPlayActivity extends BaseApplication implements IVideoPlayActivity{

    private static final String TAG = VideoPlayActivity.class.getName();
    private String videoId = "p55zRop7RWQ";
    private String mTopicName = "";
    private int topicIndex;
    private ArrayList<Topic> topicArrayList = new ArrayList<>();
    private YouTubePlayerView youTubePlayerView;
    private YouTubePlayer mYouTubePlayer;
    private YouTubePlayerTracker mTracker;
    private float playTime = 0.0f;
    private ActivityVideoPlayBinding mBinding;
    private VideoPlayViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_video_play);
        mViewModel = obtainViewModel(this);
        mBinding.setViewModel(mViewModel);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            videoId = bundle.getString(VIDEO_ID);
            topicIndex = bundle.getInt(Constants.TOPIC_INDEX);
            topicArrayList = bundle.getParcelableArrayList(Constants.TOPIC_LIST);
            mTopicName = bundle.getString(Constants.TOPIC_NAME);
            Logger.v("Topic Name : "+mTopicName);
            playTime = bundle.getFloat(Constants.START_TIME);
            Logger.v("Video Id : "+videoId);
            mBinding.setTopicIndex(topicIndex);
        }
        initViewModel();
        initYoutubePlayerView();


    }

    public static VideoPlayViewModel obtainViewModel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        VideoPlayViewModel viewModel =
                ViewModelProviders.of(activity, factory).get(VideoPlayViewModel.class);
        return viewModel;
    }

    private void initViewModel(){
        mViewModel.init(this,this, mTopicName);
    }
    private void initYoutubePlayerView(){
        youTubePlayerView = mBinding.youtubePlayerView;
        getLifecycle().addObserver(youTubePlayerView);
        mViewModel.initYoutubePlayerView(youTubePlayerView, videoId, topicIndex, topicArrayList, playTime);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        youTubePlayerView.release();
        mViewModel.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        youTubePlayerView.release();
        mViewModel.updateTotalPlayTime();
        mViewModel.updatePlayTime();

        //youTubePlayerView.release();
        //mViewModel.updatePlayTime();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mViewModel.onPause();

    }

    @NotNull
    @Override
    public Resources resources() {
        return getResources();
    }

    @Override
    public void updateTopicIndex(int topicIndex) {
        mBinding.setTopicIndex(topicIndex);
    }
}
