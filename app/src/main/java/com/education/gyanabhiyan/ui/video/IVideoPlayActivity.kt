package com.education.gyanabhiyan.ui.video

import android.content.res.Resources

interface IVideoPlayActivity {
    fun resources(): Resources
    fun updateTopicIndex(topicIndex:Int)
}