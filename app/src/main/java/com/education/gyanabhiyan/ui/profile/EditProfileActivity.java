package com.education.gyanabhiyan.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.education.gyanabhiyan.BaseApplication;
import com.education.gyanabhiyan.R;
import com.education.gyanabhiyan.databinding.ActivityEditProfileBinding;
import com.education.gyanabhiyan.model.User;
import com.education.gyanabhiyan.ui.main.MainActivity;
import com.education.gyanabhiyan.utils.ChooseDialog;
import com.education.gyanabhiyan.utils.Constants;

import java.util.Arrays;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EditProfileActivity extends BaseApplication implements IEditProfileActivity {
    @BindView(R.id.name_edittext)
    EditText nameEditText;
    @BindView(R.id.email_edittext)
    EditText emailEditText;
    @BindView(R.id.city_edittext)
    EditText cityEditText;
   /* @BindView(R.id.grade_spinner)
    Spinner gradeSpinner;*/
    @BindView(R.id.update_profile)
    Button updateProfileButton;
    private ArrayAdapter<String> genderAdapter, gradeAdapter;
    @BindArray(R.array.gender)
    String[] genders;
    @BindArray(R.array.grades)
    String[] grades;
    private ActivityEditProfileBinding mBinding;
    private EditProfileViewModel mViewModel;
    private Boolean mainActivityFlag;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        mViewModel = obtainViewModel(this);
        mBinding.setEditProfileViewModel(mViewModel);
        mBinding.setLifecycleOwner(this);
        mBinding.setIEditProfileActivity(this);
        mViewModel.iEditProfileActivity = this;
        mViewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
               /* if (user.getPhotoUrl().length() > 0)
                    Toast.makeText(getApplicationContext(), "name : " + user.getName(), Toast.LENGTH_SHORT).show();*/

            }
        });
        Bundle profileDetails = getIntent().getExtras();
        User user = new User();
        if(profileDetails != null) {
            user = profileDetails.getParcelable("user");
            int id = 0;
            mainActivityFlag = false;
        }
        else
            mainActivityFlag = true;

        mViewModel.setProfile(user);

    }

    public static EditProfileViewModel obtainViewModel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel -Not used for now
        // ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        EditProfileViewModel viewModel = ViewModelProviders.of(activity).get(EditProfileViewModel.class);
        return viewModel;
    }


    @Override
    protected void onStart() {
        super.onStart();

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBack();
    }

    public void setGrade(String grade) {
        User user = mViewModel.userMutableLiveData.getValue();
        user.grade = grade;
        mViewModel.userMutableLiveData.setValue(user);
      //  mBinding.setQuantity(grade);
    }

    @Override
    public void inflateDialog(int dialogType) {
        if (dialogType == Constants.DIALOG_GENDER)
            inflateGenderDialog(dialogType);
        else if (dialogType == Constants.DIALOG_GRADE)
            inflateGradesDialog(dialogType);

    }

    public void inflateGenderDialog(int dialogType) {
        ChooseDialog dialog = new ChooseDialog(Arrays.asList(genders),dialogType);
        dialog.show(getSupportFragmentManager(), getString(R.string.dialog_choose_gender));
    }

    public void inflateGradesDialog(int dialogType) {
        ChooseDialog dialog = new ChooseDialog(Arrays.asList(grades),1);
        dialog.show(getSupportFragmentManager(), getString(R.string.dialog_choose_grade));
    }

    @Override
    public void setDialogValue(String value, int dialogType) {
        if (dialogType == Constants.DIALOG_GENDER)
            setGender(value);
        else if (dialogType == Constants.DIALOG_GRADE)
            setGrade(value);

    }

    public void setGender(String gender){
        User user = mViewModel.userMutableLiveData.getValue();
        user.gender = gender;
        mViewModel.userMutableLiveData.setValue(user);
    }
    @Override
    public void goBack() {
        Intent intent;
        if (!mainActivityFlag)
            intent = new Intent(this,ProfileActivity.class);
        else
            intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
