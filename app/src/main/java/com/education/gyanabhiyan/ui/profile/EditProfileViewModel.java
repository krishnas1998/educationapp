package com.education.gyanabhiyan.ui.profile;

import android.app.Application;
import android.view.View;

import com.education.gyanabhiyan.R;
import com.education.gyanabhiyan.model.User;
import com.education.gyanabhiyan.utils.UserData;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import static com.education.gyanabhiyan.utils.Utils.getUid;

public class EditProfileViewModel extends AndroidViewModel       {
    public MutableLiveData<User> userMutableLiveData;
    public IEditProfileActivity iEditProfileActivity;

    public EditProfileViewModel(@NonNull Application application) {
        super(application);

    }


    LiveData<User> getUser() {
        if (userMutableLiveData == null) {
            userMutableLiveData = new MutableLiveData<>();
        }
        return userMutableLiveData;
    }
    public void setProfile(User user){
        userMutableLiveData.setValue(user);
    }

    public void updateProfile(View view) {
        final String uid = getUid();
        Map<String, Object> postValues = new HashMap<String,Object>();
        DatabaseReference profileRefrence = FirebaseDatabase.getInstance().getReference().child("users").child(uid).child("profile");
        User user = userMutableLiveData.getValue();
        postValues.put("email", user.email);
        postValues.put("name", user.getName());
        postValues.put("location", user.location);
        postValues.put("grade", user.grade);

        postValues.put("gender", user.gender);

        if(!validate(user,view))
            return;
        UserData.getInstance().setUser(user);
        profileRefrence.updateChildren(postValues);
        iEditProfileActivity.goBack();
    }

    private boolean validate(User user, View view){
        if (user.getName() == null || user.getName().isEmpty())
        {
            Snackbar.make(view,R.string.name_empty,Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (user.email == null || user.email.isEmpty())
        {
            Snackbar.make(view,R.string.email_empty,Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (user.location == null || user.location.isEmpty())
        {
            Snackbar.make(view,R.string.location_empty,Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (user.gender == null || user.gender.isEmpty())
        {
            Snackbar.make(view,R.string.gender_empty,Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (user.grade == null || user.grade.isEmpty())
        {
            Snackbar.make(view,R.string.grade_empty,Snackbar.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
}
