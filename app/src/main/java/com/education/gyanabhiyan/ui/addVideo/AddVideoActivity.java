package com.education.gyanabhiyan.ui.addVideo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.education.gyanabhiyan.R;

public class AddVideoActivity extends AppCompatActivity implements IAddVideoActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_video);
    }

    @Override
    public void inflateDialog(int dialogType) {

    }

    @Override
    public void setDialogValue(String value, int dialogType) {

    }

    @Override
    public void goBack() {

    }
}
