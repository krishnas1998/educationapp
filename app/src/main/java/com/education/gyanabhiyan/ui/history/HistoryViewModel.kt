package com.education.gyanabhiyan.ui.history

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import com.education.gyanabhiyan.adapter.HistoryAdapter
import com.education.gyanabhiyan.model.History
import com.education.gyanabhiyan.utils.firebaseRx.FireBaseDataStoreRx
import com.google.firebase.database.DatabaseReference
import com.orhanobut.logger.Logger
import kotlin.math.roundToInt

class HistoryViewModel : ViewModel() {
    lateinit var mDatabaseReference: DatabaseReference
    lateinit var mLinearLayoutManager: LinearLayoutManager
    lateinit var mContext: Context
    lateinit var mAdapter: HistoryAdapter
    private var historyList: ArrayList<History> = ArrayList()
    fun init(context: Context,linearLayoutManager: LinearLayoutManager, databaseReference: DatabaseReference) {
        mDatabaseReference = databaseReference
        mContext = context
        mLinearLayoutManager = linearLayoutManager
        mAdapter = HistoryAdapter(this)
    }

    fun getAdapter() : HistoryAdapter {
        return mAdapter
    }

    @SuppressLint("CheckResult")
    fun getHistory(){
        val fireBaseDataStoreRx: FireBaseDataStoreRx<History> = FireBaseDataStoreRx()
        val source = fireBaseDataStoreRx.dataValueEvent(History::class.java,mDatabaseReference)
        source?.subscribe({
            item: History -> Logger.d("onNext: ${item.currentSecond}")
            historyList.add(item)
        }
                , { obj: Throwable -> obj.printStackTrace() }
                ,{
            mAdapter.setHistoryList(historyList)
            mAdapter.notifyDataSetChanged()
            Logger.d("onComplete")
        }
        )
    }


    fun getHistory(index: Int):History{
        return historyList[index]
    }

    fun getProgress(index: Int):Int{
        return historyList[index].videoProgress.toFloat().roundToInt()
    }
}