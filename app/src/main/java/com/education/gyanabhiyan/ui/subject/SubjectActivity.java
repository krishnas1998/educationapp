package com.education.gyanabhiyan.ui.subject;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;

import com.education.gyanabhiyan.BaseApplication;
import com.education.gyanabhiyan.R;
import com.education.gyanabhiyan.databinding.ActivitySubjectBinding;
import com.education.gyanabhiyan.layout.PreCachingLayoutManager;
import com.education.gyanabhiyan.model.Subject;
import com.education.gyanabhiyan.ui.main.MainActivity;
import com.education.gyanabhiyan.utils.Constants;
import com.google.firebase.database.DatabaseReference;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import static com.education.gyanabhiyan.utils.DatabaseUtil.getDatabase;

public class SubjectActivity extends BaseApplication {

    private static final String TAG = SubjectActivity.class.getName();
    private LinearLayoutManager linearLayoutManager;
    private DatabaseReference mDatabase;
    private String subjectName, grade;
    private ActivitySubjectBinding mBinding;
    private Toolbar myChildToolbar;
    private ActionBar ab;
    private int mChapterIndex;
    private Subject mSubject;
    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showProgressDialog();
        setupBindings(savedInstanceState);
        setUpToolBar();
        getBundle();
        setDatabase();

//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        });
//        Observable.just("one", "two", "three", "four", "five")
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(/* an Observer */);
    }
    private SubjectViewModel mViewModel;
    private void setupBindings(Bundle savedInstanceState) {
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_subject);
        mBinding.setLifecycleOwner(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        setupViewModel();
        mViewModel.getHistory();
        hideProgressDialog();
//        AsyncTaskRunner subjectAsyc = new AsyncTaskRunner();
//        subjectAsyc.execute();
    }

    private class AsyncTaskRunner extends AsyncTask<String,String,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }

        @Override
        protected String doInBackground(String... strings) {

            return null;
        }
    }
    @Override
    protected void onPause(){
        super.onPause();
     //   mViewModel.destroy();
    }

    private void setupViewModel(){
        mViewModel = new SubjectViewModel();//ViewModelProviders.of(this).get(SubjectViewModel.class);
        mBinding.setViewModel(mViewModel);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels - getNavigationBarHeight();
        linearLayoutManager = new PreCachingLayoutManager(this,height*2);
        mViewModel.init(this, mSubject, linearLayoutManager, mChapterIndex);

        linearLayoutManager.setSmoothScrollbarEnabled(true);
      //  linearLayoutManager.scrollToPosition(mChapterIndex);
        mBinding.rvParent.setLayoutManager(linearLayoutManager);
        mBinding.rvParent.setItemAnimator(new DefaultItemAnimator());
        mBinding.rvParent.setNestedScrollingEnabled(false);
        mBinding.rvParent.setHasFixedSize(true);
        mBinding.rvParent.setItemViewCacheSize(20);
        mBinding.rvParent.getLayoutManager().scrollToPosition(mChapterIndex);
    }

    public boolean showNavigationBar(Resources resources)
    {
        int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        return id > 0 && resources.getBoolean(id);
    }

    private int getNavigationBarHeight() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            int usableHeight = metrics.heightPixels;
            getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
            int realHeight = metrics.heightPixels;
            if (realHeight > usableHeight)
                return realHeight - usableHeight;
            else
                return 0;
        }
        return 0;
    }
    private void setUpToolBar(){
        myChildToolbar = findViewById(R.id.my_child_toolbar);
        setSupportActionBar(myChildToolbar);
        // Get a support ActionBar corresponding to this toolbar
        ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
    }
    private void getBundle(){
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null)
        {
            mSubject = bundle.getParcelable("subject");
            grade = bundle.getString("grade");
            mChapterIndex = bundle.getInt(Constants.CHAPTER_INDEX);
            Log.v(TAG,"subjectName : "+mSubject.getSubjectName());
            ab.setTitle(mSubject.getSubjectName());
            // collapsingToolbarLayout.setTitle(subject);
        }
    }

    private void setDatabase(){
        mDatabase = getDatabase().getReference();
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
//        finish();
//    }

}
