package com.education.gyanabhiyan.ui.profile;

import com.education.gyanabhiyan.model.User;

public interface ProfileNavigation {
    void openEditProfileActivity(User user);
    void signOut();
}
