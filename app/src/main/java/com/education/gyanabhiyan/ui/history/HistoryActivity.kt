package com.education.gyanabhiyan.ui.history

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.education.gyanabhiyan.BaseApplication
import com.education.gyanabhiyan.R
import com.education.gyanabhiyan.databinding.ActivityHistoryBinding
import com.education.gyanabhiyan.utils.DatabaseUtil
import com.education.gyanabhiyan.utils.FirebaseUtils
import com.education.gyanabhiyan.utils.UserData

class HistoryActivity : BaseApplication() {

    lateinit var mBinding: ActivityHistoryBinding
    private val mViewModel: HistoryViewModel = HistoryViewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_history)
        mBinding.viewModel = mViewModel
        setupRecyclerViewLayout()
        getHistory()
        setupToolBar()
    }

    private fun getHistory() {
        mViewModel.getHistory()
      //  ServerValue.TIMESTAMP
    }

    private fun setupRecyclerViewLayout()
    {
        val linearLayoutManager = LinearLayoutManager(this)
        mViewModel.init(this, linearLayoutManager, DatabaseUtil.getDatabase().getReference("users").child(FirebaseUtils.getCurrentUserId()).child("history").child(UserData.getInstance().user.grade))
        linearLayoutManager.isSmoothScrollbarEnabled = true
        mBinding.rvParent.layoutManager = linearLayoutManager
        mBinding.rvParent.itemAnimator = DefaultItemAnimator()
    }

    private fun setupToolBar(){
        setSupportActionBar(mBinding.myChildToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.history)
    }
}
