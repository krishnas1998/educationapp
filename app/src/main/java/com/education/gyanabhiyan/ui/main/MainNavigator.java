package com.education.gyanabhiyan.ui.main;

import com.education.gyanabhiyan.model.Subject;

import java.util.List;

public interface MainNavigator {
    void onStarted();
    void onSuccess();
    void setProfileImage(String message);
    void showProgressDialog();
    void hideProgressDialog();
    void openProfileDialog();
    void updateUI(List<Subject> subjects, String userName);
    void openSubjectActivity(Subject subject, Integer chapterIndex, String grade, Long totalVideoTime);
    //void shareButton();
}
