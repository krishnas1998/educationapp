package com.education.gyanabhiyan.ui.profile;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.ablanco.imageprovider.ImageProvider;
import com.ablanco.imageprovider.ImageSource;
import com.education.gyanabhiyan.BaseApplication;
import com.education.gyanabhiyan.R;
import com.education.gyanabhiyan.databinding.ActivityProfileBinding;
import com.education.gyanabhiyan.model.User;
import com.education.gyanabhiyan.ui.main.MainActivity;
import com.education.gyanabhiyan.ui.signin.SignInActivity;
import com.example.flatdialoglibrary.dialog.FlatDialog;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

public class ProfileActivity extends BaseApplication implements View.OnClickListener, ProfileNavigation {

    private static final String TAG = ProfileActivity.class.getName();
    private ImageView editProfileImageView;
    private User user;
    static final int RESULT_GALLERY_IMAGE = 100;
    static final int RESULT_CAMERA_IMAGE = 101;
    private Uri Imagepath = null;
    private ImageView mPhotoImgageView;
    private boolean check = false;
    private static final int REQUEST_EXTERNAL_STORAGE = 101;
    private DatabaseReference mProfile;
    private ActivityProfileBinding mBinding;
    private ProfileViewModel mViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_profile);
        mViewModel = obtainViewModel(this);
        mBinding.setProfileViewModel(mViewModel);
        mViewModel.getProfileDetails();
        mViewModel.mProfileNavigation = this;
        mBinding.setLifecycleOwner(this);

        mViewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
            }
        });
        editProfileImageView = findViewById(R.id.editprofiledetails);

        //phoneNoTextView = findViewById(R.id.phoneno);
        mPhotoImgageView = findViewById(R.id.profile_image);
        mPhotoImgageView.setOnClickListener(this);
        editProfileImageView.setOnClickListener(this);
        mProfile = FirebaseDatabase.getInstance().getReference().child("users").child(getUid()).child("profile");
       // getProfileDetails();

        // my_child_toolbar is defined in the layout file
        Toolbar myChildToolbar =
                (Toolbar) findViewById(R.id.my_child_toolbar);
        setSupportActionBar(myChildToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

    }

    public static ProfileViewModel obtainViewModel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel -Not used for now
       // ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());

        ProfileViewModel viewModel =
                ViewModelProviders.of(activity).get(ProfileViewModel.class);

        return viewModel;
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.sign_out:
                break;
            case R.id.profile_image:
                ChangeProfilePic();
                break;

        }
    }

    @Override
    public void signOut() {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(this,SignInActivity.class );
        startActivity(intent);
        finish();
    }

    private void ChangeProfilePic() {
       // TakePhoto();
        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            TakePhoto();
        } else {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                TakePhoto();
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(getContext(), "External Storage and Camera permission is required to read images from device", Toast.LENGTH_SHORT).show();
                }
                //requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE);
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA}, REQUEST_EXTERNAL_STORAGE);
            }
        }

    }


    private void TakePhoto() {
        Toast.makeText(getContext(), "Going to change pic", Toast.LENGTH_SHORT).show();

        final FlatDialog flatDialog = new FlatDialog(this);
        flatDialog.setTitle("Choose option")
                //.setSubtitle("write your profile info here")
//                .setFirstTextFieldHint("email")
//                .setSecondTextFieldHint("password")
                .setFirstButtonText("Choose From Gallery")
                .setSecondButtonText("Use Camera")
                .setThirdButtonText("Cancel")
                .withFirstButtonListner(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent galleryintent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryintent, RESULT_GALLERY_IMAGE);
                        flatDialog.dismiss();
                    }
                })
                .withSecondButtonListner(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openCamera();
                        flatDialog.dismiss();
                    }
                })
                .withThirdButtonListner(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        flatDialog.dismiss();
                    }
                })
                .show();

    }

    ImageProvider imageProvider = new ImageProvider(this);

    private void openCamera()
    {
        imageProvider.getImage(ImageSource.CAMERA,bitmap -> {
            if (bitmap == null)
                return null;
            Uri imageUri = getImageUri(this,bitmap);
            UploadPhoto(imageUri);
            mBinding.profileImage.setImageBitmap(bitmap);
            return null;
        });
    }

        @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        check = true;
        if (requestCode == RESULT_GALLERY_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Imagepath = data.getData();
            InputStream inputstream;
            try {
                inputstream = getContentResolver().openInputStream(Imagepath);
                Bitmap bitmap = BitmapFactory.decodeStream(inputstream);
                bitmap = Bitmap.createScaledBitmap(bitmap, 710, 710, true);
                mBinding.profileImage.setImageBitmap(bitmap);
                //mPhotoImgageView.setImageBitmap(bitmap);
                UploadPhoto(Imagepath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


        }
    }
    private Context getContext() {
        return ProfileActivity.this;
    }

    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(img, selectedImage);
        return img;
    }
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }
    private static Bitmap rotateImageIfRequired(Bitmap img, Uri  selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    private void UploadPhoto(Uri Imagepathq) {
        changeProfilePic(Imagepathq);
    }

    public void changeProfilePic(Uri imagepath) {

        final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("profile_images").child(getUid());
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getApplication().getContentResolver(), imagepath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = filePath.putBytes(data);
      //  UploadTask uploadTask = filePath.putFile(imagepath);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                double progress = (100.0 * task.getResult().getBytesTransferred()) / task.getResult().getTotalByteCount();
                Log.v(TAG,"Progress : " + progress);
                progressDialog.setMessage("Uploaded "+(int)progress+"%");
                // Continue with the task to get the download URL
                return filePath.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    Map newImage = new HashMap();
                    Log.v(TAG,"photoUrl : " + downloadUri.toString());
                    Snackbar.make(findViewById(R.id.root_layout),R.string.profilephotoupdated,Snackbar.LENGTH_SHORT).show();
                    newImage.put("photoUrl", downloadUri.toString());
                    mProfile.updateChildren(newImage);
                    progressDialog.dismiss();
                    //mBinding.profileImage.setImageBitmap(bitmap);
                } else {
                    // Handle failures
                    // ...
                    progressDialog.dismiss();
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void openEditProfileActivity(User user) {
        Intent intent = new Intent(this,EditProfileActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("user",user);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }
}
