package com.education.gyanabhiyan.interfaces;

public interface IDialog {
    void inflateDialog(int dialogType);
    void setDialogValue(String value, int dialogType);
    void goBack();
}
