package com.education.gyanabhiyan.interfaces;

import android.net.Uri;


public interface UpdateProfileInteractor {

    void updateProfile(String name, String phone, String city, OnUpdateFinishedListener listener);

    void getProfile();

    void changeProfilePic(Uri imagepath, OnUpdateFinishedListener listener);

    interface OnUpdateFinishedListener {
        void onUsernameError();

        void onPhoneError();

        void onCompanyError();

        void onDesignationdError();

        void onCertificateError();

        void onCityError();

        void onSuccess();

        void onError();
    }

}

