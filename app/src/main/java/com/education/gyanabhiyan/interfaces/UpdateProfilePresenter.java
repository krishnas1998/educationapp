package com.education.gyanabhiyan.interfaces;

import android.net.Uri;

import com.education.gyanabhiyan.model.User;

public interface UpdateProfilePresenter {


    void validateCredentials(String name, String phone, String city);

    void getProfile(User user);

    void requestCurrentDetails();

    void onDestroy();

    void updatePhoto(Uri imagePath);
}
