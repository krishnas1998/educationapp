package com.education.gyanabhiyan.interfaces;

import com.education.gyanabhiyan.model.User;

public interface UpdateProfileView {

    void setUsernameError();

    void setPhoneError();

    void setCityError();

    void navigateToHome();

    void setUser(User user);

    void onSuccess();

    void onError();

}