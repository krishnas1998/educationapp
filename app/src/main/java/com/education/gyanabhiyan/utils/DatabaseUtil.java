package com.education.gyanabhiyan.utils;

import com.google.firebase.database.FirebaseDatabase;

public class DatabaseUtil {

    private static FirebaseDatabase mDatabase;
    public static FirebaseDatabase getDatabase() {
        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
        }

        return mDatabase;
    }
}
