package com.education.gyanabhiyan.utils;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.education.gyanabhiyan.databinding.ChooseDialogBinding;
import com.education.gyanabhiyan.interfaces.IDialog;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class ChooseDialog extends DialogFragment {

    private static final String TAG = "ChooseQuantityDialog";

    // data binding
    ChooseDialogBinding mBinding;
    private List<String> items;
    private int DialogType;
    public ChooseDialog(List<String> items,int DialogType)
    {
        this.items = items;
        this.DialogType = DialogType;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = ChooseDialogBinding.inflate(inflater);
        ArrayAdapter<String> itemsAdapter =

                new ArrayAdapter<String>(mBinding.getRoot().getContext(), android.R.layout.simple_list_item_1, items);
        mBinding.listView.setAdapter(itemsAdapter);
        mBinding.listView.setOnItemClickListener(mOnItemClickListener);
        mBinding.closeDialog.setOnClickListener(mCloseDialogListener);

        return mBinding.getRoot();
    }

    public AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Log.d(TAG, "onItemSelected: selected: " + adapterView.getItemAtPosition(i));
            IDialog iMainActivity = (IDialog) getActivity();
            iMainActivity.setDialogValue(adapterView.getItemAtPosition(i).toString(), DialogType);
            getDialog().dismiss();
        }
    };

    public View.OnClickListener mCloseDialogListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getDialog().dismiss();
        }
    };
}