package com.education.gyanabhiyan.utils.firebaseRx

import com.google.firebase.database.*
import com.orhanobut.logger.Logger
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class FireBaseDataStoreRx<T> {

    /**
     * @param exampleClass  for parsing data into
     * @param queryDatabase to attach child listener
     * @return [Observable] of child listener
     */
    fun dataValueEvent(exampleClass: Class<T>?, queryDatabase: Query?): Observable<T>? {
        return Observable.create { emitter: ObservableEmitter<T> ->
            queryDatabase?.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    Logger.e("cancelled")
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (postDataSnapShot: DataSnapshot in dataSnapshot.children){
                        val tObject = exampleClass?.let { postDataSnapShot.getValue(it) }
                        tObject?.let {
                            emitter.onNext(it)
                        }
                    }
                    emitter.onComplete()
                }

            })

        }
    }

    fun dataChildEvent(exampleClass: Class<T>?, queryDatabase: Query?): Observable<T>? {
        return Observable.create { emitter: ObservableEmitter<T> ->
            queryDatabase?.addChildEventListener(object : ChildEventListener {
                override fun onCancelled(dataSnapshot: DatabaseError) {
                    Logger.e("cancelled")
                }

                override fun onChildMoved(dataSnapshot: DataSnapshot, p1: String?) {
                    Logger.d("Child moved")
                }

                override fun onChildChanged(dataSnapshot: DataSnapshot, p1: String?) {
                    Logger.d("onChildChanged")
                }

                override fun onChildAdded(dataSnapshot: DataSnapshot, p1: String?) {
                    Logger.d("onChildAdded")
                    for (postDataSnapShot: DataSnapshot in dataSnapshot.children){
                        val tObject = exampleClass?.let { postDataSnapShot.getValue(it) }
                        tObject?.let {
                            emitter.onNext(it)
                        }
                    }
                    emitter.onComplete()
                }

                override fun onChildRemoved(p0: DataSnapshot) {
                    Logger.d("onChildRemoved")
                }

            })

        }.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /*operator fun get(url: String?, context: Context): Completable? {
        return Completable.create { emitter ->
            Glide
                    .with(context)
                    .load(url)
            emitter.onComplete()
        }
    }*/
}