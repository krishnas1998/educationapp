package com.education.gyanabhiyan.utils;

public class Constants {
    public static final int DIALOG_GENDER = 0;
    public static final int DIALOG_GRADE = 1;
    public static final int DIALOG_SUBJECTS = 2;
    //Intents constants
    public static final String TOPIC_LIST = "TOPIC_LIST";
    public static final String VIDEO_ID = "VIDEO_ID";
    public static final String TOPIC_INDEX = "TOPIC_INDEX";
    public static final String CHAPTER_INDEX = "CHAPTER_INDEX";
    public static final String TOPIC_NAME = "TOPIC_NAME";
    public static final String START_TIME = "START_TIME";
}
