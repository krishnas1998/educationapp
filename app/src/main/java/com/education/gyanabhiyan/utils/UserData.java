package com.education.gyanabhiyan.utils;

import com.education.gyanabhiyan.model.User;

public class UserData {
    private static UserData single_instance = null;

    // variable of type String
    public User user;

    // private constructor restricted to this class itself
    private UserData()
    {
        //s = "Hello I am a string part of Singleton class";
    }

    // static method to create instance of Singleton class
    public static UserData getInstance()
    {
        if (single_instance == null)
            single_instance = new UserData();

        return single_instance;
    }

    public void setUser(User user){
        this.user = user;
    }
}
