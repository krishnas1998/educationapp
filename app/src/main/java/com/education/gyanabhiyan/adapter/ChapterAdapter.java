package com.education.gyanabhiyan.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.education.gyanabhiyan.databinding.ChapterParentRecyclerviewBinding;
import com.education.gyanabhiyan.model.Chapter;
import com.education.gyanabhiyan.model.Subject;
import com.education.gyanabhiyan.ui.subject.SubjectViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.MyChildView>  {

    private List<Chapter> chapterList = new ArrayList<>();
    private TopicAdapter topicAdapter;
    private SubjectViewModel viewModel;
    private int layoutParent;
    private int layoutChild;

    public ChapterAdapter(Subject subject, SubjectViewModel subjectViewModel, int layoutParent, int layoutChild)    {
        viewModel = subjectViewModel;
        this.layoutParent = layoutParent;
        this.layoutChild = layoutChild;
        chapterList = subject.getChapters();
        viewModel.addChapter(subject.getChapters());
        this.notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MyChildView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ChapterParentRecyclerviewBinding itemView = DataBindingUtil.inflate(layoutInflater,layoutParent,viewGroup,false);
        return new MyChildView(itemView);
    }

    public TopicAdapter getTopicAdapter(){
        return topicAdapter;
    }
    @Override
    public void onBindViewHolder(@NonNull MyChildView myChildView, int chapterIndex) {
        topicAdapter = new TopicAdapter(chapterList.get(chapterIndex).getChapterTopics(),viewModel, chapterIndex, layoutChild);
        myChildView.bind(viewModel,chapterIndex);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull MyChildView holder) {
        super.onViewAttachedToWindow(holder);
        if (holder.getAdapterPosition() == 0)
        holder.binding.rvChild.requestFocus();
    }

    @Override
    public int getItemCount() {
        return chapterList == null ? 0 : chapterList.size();
    }

    public class MyChildView extends RecyclerView.ViewHolder {
        final ChapterParentRecyclerviewBinding binding;
        public MyChildView(@NonNull ChapterParentRecyclerviewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.rvChild.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext(), RecyclerView.HORIZONTAL,false));
        }

        void bind(SubjectViewModel viewModel,Integer position){
            binding.setViewModel(viewModel);
            binding.setChapterIndex(position);
            binding.executePendingBindings();
        }
    }
}
