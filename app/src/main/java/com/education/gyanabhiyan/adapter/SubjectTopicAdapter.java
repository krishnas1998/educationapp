package com.education.gyanabhiyan.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.education.gyanabhiyan.databinding.SubjectChapterChildRecyclerviewBinding;
import com.education.gyanabhiyan.model.Chapter;
import com.education.gyanabhiyan.ui.main.MainViewModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class SubjectTopicAdapter extends RecyclerView.Adapter<SubjectTopicAdapter.MyChildView>  {
    private ArrayList<Chapter> chaptersList;
    private MainViewModel viewModel;
    private Integer chapterIndex;
    private int layoutChild;

    public SubjectTopicAdapter(ArrayList<Chapter> horizontalList, MainViewModel viewModel, Integer chapterIndex, int layoutChild)    {
        this.chaptersList = horizontalList;
        this.viewModel = viewModel;
        this.layoutChild = layoutChild;
        this.chapterIndex = chapterIndex;
        this.notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MyChildView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        SubjectChapterChildRecyclerviewBinding itemView = DataBindingUtil.inflate(layoutInflater, layoutChild, viewGroup, false);
        return new MyChildView(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyChildView myChildView, final int i) {
        myChildView.bind(viewModel,i);
    }

    @Override
    public int getItemCount() {
        return chaptersList.size();
    }

    public class MyChildView extends RecyclerView.ViewHolder {
        private SubjectChapterChildRecyclerviewBinding binding;
        public MyChildView(@NonNull SubjectChapterChildRecyclerviewBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(MainViewModel viewModel,Integer topicIndex){
            binding.setMainViewModel(viewModel);
            binding.setChapterIndex(chapterIndex);
            binding.setTopicIndex(topicIndex);
        }
    }
}
