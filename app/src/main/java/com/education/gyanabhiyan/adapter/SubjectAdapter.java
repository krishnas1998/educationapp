package com.education.gyanabhiyan.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.education.gyanabhiyan.databinding.SubjectParentRecyclerviewBinding;
import com.education.gyanabhiyan.model.Subject;
import com.education.gyanabhiyan.ui.main.MainViewModel;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.MyChildView>  {

    private final String TAG = SubjectAdapter.class.getName();
    private SubjectTopicAdapter subjectTopicAdapter;
    private MainViewModel viewModel;
    private int layoutParent;
    private int layoutChild;
    private List<Subject> subjects;
    public SubjectAdapter(DatabaseReference database, List<Subject> subjects, MainViewModel MainViewModel, int layoutParent, int layoutChild)    {
        viewModel = MainViewModel;
        this.layoutParent = layoutParent;
        this.layoutChild = layoutChild;
        /*this.subjects = subjects;
        Subject subject = new Subject("1",new ArrayList<>());
      */
        this.notifyDataSetChanged();
    }

    public void setSubjects(List<Subject> subjects){
        this.subjects = subjects;
    }
    @NonNull
    @Override
    public MyChildView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        SubjectParentRecyclerviewBinding itemView = DataBindingUtil.inflate(layoutInflater,i,viewGroup,false);
        //itemView.rvChild.scrollToPosition(5);
        return new MyChildView(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);
    }

    private int getLayoutIdForPosition(int position) {
        return layoutParent;
    }
    /* public void addSubject(Subject subject){
            subjects.add(subject);
            notifyItemInserted(subjects.size());
        }*/
    public SubjectTopicAdapter getTopicAdapter(){
        return subjectTopicAdapter;
    }
    @Override
    public void onBindViewHolder(@NonNull MyChildView myChildView, int subjectIndex) {
        subjectTopicAdapter = new SubjectTopicAdapter(subjects.get(subjectIndex).getChapters(),viewModel, subjectIndex, layoutChild);
        myChildView.bind(viewModel,subjectIndex);
    }

    @Override
    public int getItemCount() {
        return subjects == null ? 0 : subjects.size();
    }

    public class MyChildView extends RecyclerView.ViewHolder {
        final SubjectParentRecyclerviewBinding binding;
        //final ViewDataBinding binding;
        public MyChildView(@NonNull SubjectParentRecyclerviewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.subjectTopicRv.setLayoutManager(new LinearLayoutManager(binding.getRoot().getContext(), RecyclerView.HORIZONTAL,false));
        }

        void bind(MainViewModel viewModel, Integer position){
            binding.setMainViewModel(viewModel);
            binding.setChapterIndex(position);
            //binding.setChapterIndex(position);
            binding.executePendingBindings();
       //     binding.rvChild.scrollToPosition(5);
        //    binding.rvChild.smoothScrollToPosition(5);
        }
    }
}
