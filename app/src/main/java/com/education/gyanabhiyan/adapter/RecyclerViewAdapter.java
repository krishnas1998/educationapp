package com.education.gyanabhiyan.adapter;

/**
 * Created by krishna on 31/8/17.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.education.gyanabhiyan.R;
import com.education.gyanabhiyan.ui.video.VideoPlayActivity;

import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import static android.content.ContentValues.TAG;

/**
 * Created by Juned on 3/27/2017.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyView> {

    private List<String> list;
    private  int i =0;
    private Context mContext;
    private int selectedPosition = -1;
    private int lastPosition = -1;
    private String Name ;



    public class MyView extends RecyclerView.ViewHolder {

        private TextView textView;
        private CardView cardView;
      //  private EditText tendAmount;
      //  private TextView BalanceAmount;
      //  private SparseBooleanArray selectedItems = new SparseBooleanArray();

        public MyView(View view) {
            super(view);

            textView = view.findViewById(R.id.textview1);
            cardView = view.findViewById(R.id.cardview);
        //    tendAmount = view.findViewById(R.id.TendAmount);
       //     BalanceAmount = view.findViewById(R.id.balanceAmount);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }
    }


    public RecyclerViewAdapter(Context context,String Name,List<String> horizontalList)//, List<String> otherList)
     {
        this.list = horizontalList;
        this.Name = Name;
       // helperList = otherList;
        mContext = context;
        this.notifyDataSetChanged();
    }

    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_chapter, parent, false);
//        this.notifyDataSetChanged();

        return new MyView(itemView);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {
        holder.textView.setText(list.get(position));
        //holder.cardView.setSelected((selectedItems.get(position, false)));
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                curr = position;
//                holder.cardView.setCardBackgroundColor(ContextCompat.getColor(mContext,R.color.cardview_selected));
                Log.i(TAG, "onClick: "+position);
                Log.i(TAG, "List: "+list.get(position));
                Intent intent = new Intent(mContext, VideoPlayActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("topic",list.get(position));
                intent.putExtras(bundle);
                mContext.startActivity(intent);
                ///hiiii
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
