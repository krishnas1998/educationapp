package com.education.gyanabhiyan.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.education.gyanabhiyan.databinding.ChapterChildRecyclerviewBinding;
import com.education.gyanabhiyan.model.Topic;
import com.education.gyanabhiyan.ui.subject.SubjectViewModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.MyChildView>  {
    private ArrayList<Topic> topicList;
    private SubjectViewModel viewModel;
    private Integer chapterIndex;
    private int layoutChild;

    public TopicAdapter(ArrayList<Topic> horizontalList, SubjectViewModel viewModel, Integer chapterIndex, int layoutChild)    {
        this.topicList = horizontalList;
        this.viewModel = viewModel;
        this.layoutChild = layoutChild;
        this.chapterIndex = chapterIndex;
        this.notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MyChildView onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        ChapterChildRecyclerviewBinding itemView = DataBindingUtil.inflate(layoutInflater, layoutChild, viewGroup, false);
        return new MyChildView(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyChildView myChildView, final int i) {
        myChildView.bind(viewModel,i);
    }

    @Override
    public int getItemCount() {
        return (topicList == null ? 0:topicList.size());
    }

    public class MyChildView extends RecyclerView.ViewHolder {
        private ChapterChildRecyclerviewBinding binding;
        public MyChildView(@NonNull ChapterChildRecyclerviewBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
        }

        public void bind(SubjectViewModel viewModel,Integer topicIndex){
            binding.setViewModel(viewModel);
            binding.setChapterIndex(chapterIndex);
            binding.setTopicIndex(topicIndex);
        }
    }
}
