package com.education.gyanabhiyan.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.education.gyanabhiyan.R
import com.education.gyanabhiyan.databinding.HistoryChildRecyclerviewBinding
import com.education.gyanabhiyan.model.History
import com.education.gyanabhiyan.ui.history.HistoryViewModel
import java.util.*

class HistoryAdapter(viewModel: HistoryViewModel): RecyclerView.Adapter<HistoryAdapter.MyChildView>() {
    private var mViewModel: HistoryViewModel = viewModel
    private var mHistoryList:ArrayList<History> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyChildView {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = DataBindingUtil.inflate<HistoryChildRecyclerviewBinding>(layoutInflater, R.layout.history_child_recyclerview, parent, false)
        return MyChildView(itemView)
    }

    override fun getItemCount(): Int {
        return mHistoryList.size;
    }

    override fun onBindViewHolder(holder: MyChildView, historyIndex: Int) {
        holder.bind(mViewModel, historyIndex)
    }

    fun setHistoryList(historyList: ArrayList<History>) {
        mHistoryList = historyList
    }

    class MyChildView(val binding: HistoryChildRecyclerviewBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(viewModel: HistoryViewModel?, position: Int?) {
            binding.viewModel = viewModel
            binding.historyIndex = position
            binding.executePendingBindings()
        }

        init {
           // binding.rvChild.layoutManager = LinearLayoutManager(binding.root.context, RecyclerView.HORIZONTAL, false)
        }
    }
}