package com.education.gyanabhiyan.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;


// [START blog_user_class]
@IgnoreExtraProperties
public class User extends BaseObservable implements Parcelable {

    private String name;
    public String email;
    public String gender;
    public String location;
    public String grade;
    public String mobileno;
    public String photoUrl;

    public User() {
        photoUrl = "";
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }



    @Override
    public int describeContents() {
        return 0;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.email);
        parcel.writeString(this.gender);
        parcel.writeString(this.location);
        parcel.writeString(this.grade);
        parcel.writeString(this.mobileno);
        parcel.writeString(this.photoUrl);

    }

    protected User(Parcel in) {
        this.name = in.readString();
        this.email = in.readString();
        this.gender = in.readString();
        this.location = in.readString();
        this.grade = in.readString();
        this.mobileno = in.readString();
        this.photoUrl = in.readString();
    }

    public User(String name, String email, String gender, String location, String grade, String mobileno, String photoUrl) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.location = location;
        this.grade = grade;
        this.mobileno = mobileno;
        this.photoUrl = photoUrl;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Bindable
    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(com.education.gyanabhiyan.BR.name);
    }

//    @BindingAdapter("profileImage")
//    public static void loadImage(ImageView view, String imageUrl) {
//        Glide.with(view.getContext())
//                .load(imageUrl).apply(new RequestOptions().circleCrop())
//                .into(view);
//    }
}