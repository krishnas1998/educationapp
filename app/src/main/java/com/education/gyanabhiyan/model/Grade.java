package com.education.gyanabhiyan.model;

import java.util.ArrayList;

import androidx.annotation.Keep;
import androidx.annotation.Nullable;
@Keep
public class Grade {
    private String gradeName;
    private ArrayList<Subject> subjects;

    public String getGradeName() {
        return gradeName;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        //if ()
        String grade = obj.toString().toLowerCase();
        return grade.equals(this.gradeName.toLowerCase())? true: false;
    }

    public ArrayList<Subject> getSubjects() {
        return subjects;
    }

    public Grade() {
    }

    public Grade(String gradeName, ArrayList<Subject> subjects) {
        this.gradeName = gradeName;
        this.subjects = subjects;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

    public void setSubjects(ArrayList<Subject> subjects) {
        this.subjects = subjects;
    }
}
