package com.education.gyanabhiyan.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Chapter implements Parcelable {
    private String chapterName;
    public Chapter(String chapterName, ArrayList<Topic> chapterTopics) {
        this.chapterName = chapterName;
        this.chapterTopics = chapterTopics;
    }

    public Chapter() {
    }

    protected Chapter(Parcel in) {
        chapterName = in.readString();
        if (chapterTopics == null) chapterTopics = new ArrayList<>();
        in.readTypedList(chapterTopics, Topic.CREATOR);
    }

    public static final Creator<Chapter> CREATOR = new Creator<Chapter>() {
        @Override
        public Chapter createFromParcel(Parcel in) {
            return new Chapter(in);
        }

        @Override
        public Chapter[] newArray(int size) {
            return new Chapter[size];
        }
    };

    public String getChapterName() {
        return chapterName;
    }

    public ArrayList<Topic> getChapterTopics() {
        return chapterTopics;
    }

    private ArrayList<Topic> chapterTopics;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(chapterName);
        dest.writeTypedList(chapterTopics);
    }
}
