package com.education.gyanabhiyan.model;

import java.util.ArrayList;

public class Grades{
    private ArrayList<Grade> grades;

    public ArrayList<Grade> getGrades() {
        return grades;
    }

    public void setGrades(ArrayList<Grade> grades) {
        this.grades = grades;
    }

    public Grades() {
    }

    public Grades(ArrayList<Grade> grades) {
        this.grades = grades;
    }
}