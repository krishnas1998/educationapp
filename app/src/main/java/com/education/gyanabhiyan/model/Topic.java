package com.education.gyanabhiyan.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.education.gyanabhiyan.R;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.BindingAdapter;

public class Topic implements Parcelable {
    protected Topic(Parcel in) {
        topicTitle = in.readString();
       // topicImgUrl = in.readString();
        topicVideoId = in.readString();
        if (topicTagIds == null) topicTagIds = new ArrayList<>();
        in.readStringList(topicTagIds);

    }

    public static final Creator<Topic> CREATOR = new Creator<Topic>() {
        @Override
        public Topic createFromParcel(Parcel in) {
            return new Topic(in);
        }

        @Override
        public Topic[] newArray(int size) {
            return new Topic[size];
        }
    };

    public void setTopicTitle(String topicTitle) {
        this.topicTitle = topicTitle;
    }

    public void setTopicImgUrl(String topicImgUrl) {
        this.topicImgUrl = topicImgUrl;
    }

    private String topicTitle;
    private String topicImgUrl;
    private String topicVideoId;
    private List<String> topicTagIds;

    public void setTopicVideoId(String topicVideoId) {
        this.topicVideoId = topicVideoId;
    }

    public List<String> getTopicTagIds() {
        return topicTagIds;
    }

    public void setTopicTagIds(List<String> topicTagIds) {
        this.topicTagIds = topicTagIds;
    }

    public String getTopicImgUrl() {
        return topicImgUrl;
    }

    public String getTopicVideoId() {
        return topicVideoId;
    }

    public Topic(String topicTitle, String topicImgUrl, String topicVideoId, List<String> topicTagIds) {
        this.topicTitle = topicTitle;
      //  this.topicImgUrl = topicImgUrl;
        this.topicVideoId = topicVideoId;
        this.topicTagIds = topicTagIds;
    }

    public Topic() {
    }

    public String getTopicTitle() {
        return topicTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(topicTitle);
      //  dest.writeString(topicImgUrl);
        dest.writeString(topicVideoId);
        dest.writeStringList(topicTagIds);
    }

    @SuppressLint("CheckResult")
    @BindingAdapter({"bind:videoImg"})
    public static void loadImage(ImageView view, String topicVideoId) {
        Glide.with(view.getContext())
                .load("https://img.youtube.com/vi/"+topicVideoId+"/mqdefault.jpg")
                .placeholder(R.drawable.gyan_abhiyan_logo)
                .encodeQuality(50)
                .into(view);
    }
}
