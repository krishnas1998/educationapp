package com.education.gyanabhiyan.model

data class History(var subject:String = "",var videoDuration:String = "",var videoId:String = "",var videoProgress: String = "", var videoTitle: String = "", var currentSecond: String = "", var timeStamp: Any = "")