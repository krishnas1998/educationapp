package com.education.gyanabhiyan.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Subject implements Parcelable {
    private String subjectName;
    private ArrayList<Chapter> chapters;

    public Subject() {
    }

    protected Subject(Parcel in) {
        subjectName = in.readString();
        if (chapters == null){ chapters = new ArrayList<>(); }
        in.readTypedList(chapters, Chapter.CREATOR);
    }

    public static final Creator<Subject> CREATOR = new Creator<Subject>() {
        @Override
        public Subject createFromParcel(Parcel in) {
            return new Subject(in);
        }

        @Override
        public Subject[] newArray(int size) {
            return new Subject[size];
        }
    };

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public ArrayList<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(ArrayList<Chapter> chapters) {
        this.chapters = chapters;
    }

    public Subject(String subjectName, ArrayList<Chapter> chapters) {
        this.subjectName = subjectName;
        this.chapters = chapters;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(subjectName);
        dest.writeTypedList(chapters);
    }
}
